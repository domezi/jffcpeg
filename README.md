# What is jffcpeg?

When running the cutting software "DaVinci Resolve" in Linux, which you can do, you won't have access to the common H.264 codec. Therefore, you have to convert all your footage to Quicktime.
This tool helps you to do that, by converting footage to exactly the format Resolve expects. You don't have to worry about anything.


# GETTING STARTED / INSTALLATION (jffcpeg)
## 0. System requirements

Make sure, you are running some sort of linux

## 1. Install dependencies

```
sudo apt-get install -y python3 python3-pip ffmpeg python3-pyqt5  
```

## 2. Clone repo

```
git clone https://gitlab.com/domezi/jffcpeg.git && cd jffcpeg
```

## 3. Run it

```
python3 encoder.py
```



## Please report bugs and thoughts
Please feel free to report anything at https://gitlab.com/domezi/jffcpeg/issues/new
