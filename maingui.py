from PyQt5 import QtWidgets, uic
import sys

l_msg = None

def b_okay():
    print("okay!")
    msg("Alright.")

def b_no():
    print("no!")
    msg("I'm sorry, do you want me to remember this choice?")
    msg("May I transcode the following clips and move them to Team C?:\n\nM0342324.MOV\nM04302432.MOV")

def b_exit_ai():
    print("exit ai!")
    window.close()

def msg(t_msg): 
    if t_msg == "":
        t_msg = "Es liegen keine Meldungen vor."
    l_msg.setText(t_msg)

class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('maingui.ui', self) # Load the .ui file

        self.b_okay = self.findChild(QtWidgets.QPushButton, 'b_okay')
        self.b_okay.clicked.connect(b_okay);

        self.b_no = self.findChild(QtWidgets.QPushButton, 'b_no')
        self.b_no.clicked.connect(b_no);

        self.b_exit_ai = self.findChild(QtWidgets.QPushButton, 'b_exit_ai')
        self.b_exit_ai.clicked.connect(b_exit_ai);
    
        global l_msg
        l_msg = self.findChild(QtWidgets.QTextBrowser, 'l_msg')

        self.show() # Show the GUI

app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
window = Ui() # Create an instance of our class
app.exec_() # Start the application

