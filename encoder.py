#from PyQt5.QtWidgets import QLabel, QMessageBox, QApplication, QWidget, QPushButton, QVBoxLayout, QFileDialog, QListWidgeAt
from PyQt5.QtWidgets import *
from PyQt5 import *
from PyQt5.QtCore import QFileInfo
import subprocess
import os
from threading import Thread
import json

# vars
files = []
files_queue = []
files_processed = []
outdir = ""


# start
app = QApplication(["Resolve Footage Converter & Manager for Linux"])

# main window
window = QWidget()
window.resize(600,600)
layout = QVBoxLayout()
layout.addWidget(QLabel("Files which are going to be converted:"))

# create list
listWidget = QListWidget()
listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

def popcache():
    #remember files in queue
    global files
    try:
        with open(".cache_jffcpeg.json", 'r') as j:
            cache = json.loads(".cache_jffcpeg.json")
            files=cache["files"]
    except:
        print("couldn't load cache")


popcache()

def pushcache():
    #remember files in queue
    cache = {"v":1,"files":files}
    with open('.cache_jffcpeg.json', 'w') as f:
        json.dump(cache, f)
    print("cache written")

def refresh_progress():
    list_queue.clear()
    list_processed.clear()
    for file in files_queue:
        list_queue.addItem(file)
    for file in files_processed:
        list_processed.addItem(file)


def refresh_files():
    listWidget.clear()
    for file in files:
        listWidget.addItem(file)
    #pushcache()

def refresh_outdir():
    if outdir == "":
        label_outdir.setText("Same directory as source")
    else:
        label_outdir.setText(outdir)
    print("setting outdir: "+outdir)


def addfiles():
    dialog = QFileDialog()
    dialog_files = dialog.getOpenFileNames()[0]

    for dialog_file in dialog_files:
        files.append(dialog_file)
      
    refresh_files()


def delfiles():
    for item in listWidget.selectedItems():
        files.remove(item.text())
    refresh_files()

def change_outdir():
    dialog = QFileDialog()
    global outdir
    outdir = dialog.getExistingDirectory()
    refresh_outdir()

sizes = ["1280:720","1920:1080","3840:2160"]

# progress window
window_progress = QWidget()
window_progress.resize(500,300)
list_queue = QListWidget()
list_processed = QListWidget()
b_cancel = QPushButton("Cancel conversion")
b_cancel.setShortcut("esc")

def closeprogress():
    window_progress.close()
b_cancel.clicked.connect(closeprogress)

def initprogress():
    l = QVBoxLayout()
    l.addWidget(QLabel("Queued files:"))
    l.addWidget(list_queue)
    l.addWidget(QLabel("Processed files:"))
    l.addWidget(list_processed)
    l.addWidget(b_cancel)
    window_progress.setLayout(l)

initprogress()

def showprogress():
    window_progress.show()
    refresh_progress()

def convertall():
    print("starting conversion ...")
    print("current"+str(button_settings.currentIndex()))

    b_cancel.setText("Cancel conversion")
    global files_queue,files_processed
    files_queue=files.copy()
    files_processed=[]
    showprogress()

    t = Thread(target = lambda: showprogress())
    t.start()
    t.join()

    QApplication.processEvents()
    app.processEvents()

    for dialog_file in files.copy():

        basename=os.path.basename(dialog_file)
        basename_new=os.path.splitext(basename)[0]+".mov"
        dirname=os.path.dirname(dialog_file)

        if outdir == "":
            newname = dirname+"/T_"+basename_new
        else:
            newname = outdir+"/T_"+basename_new

        print(newname)

        #command = "ffmpeg -y -i \""+dialog_file+"\" -acodec pcm_s16le  -c:v dnxhd -vf \"scale="+str(sizes[button_settings.currentIndex()])+",fps=30000/1001,format=yuv422p\" -b:v 110M -c:a pcm_s16le \""+newname+"\""
        command = "ffmpeg -y -i \""+dialog_file+"\" -vcodec dnxhd -acodec pcm_s16le -s "+str(sizes[button_settings.currentIndex()])+" -r 30000/1001 -b:v 36M -pix_fmt yuv422p -f mov \""+newname+"\""

        QApplication.processEvents()
        app.processEvents()

        print("\n\n"+command+"\n\n")
        #os.system(command)
        #output = os.popen(command).readlines()
        #output = subprocess.call(command, shell=True)  # returns the exit code in unix
        t = Thread(target = lambda: os.system(command))
        t.start()

        files_queue.remove(dialog_file)
        files_processed.append(dialog_file)
        refresh_progress()

        #t2 = Thread(target = lambda: refresh_progress())
        #t2.start()

        files.remove(dialog_file)
        refresh_files()

        t.join()
        print("##################################### DONE ###############################")

    b_cancel.setText("Close")

button_add = QPushButton('&Import files ...')
button_add.clicked.connect(addfiles)
button_add.setShortcut("ctrl+i")
button_add.setToolTip("Import files (Ctrl+I)")
button_del = QPushButton('&Remove selected')
button_del.clicked.connect(delfiles)
button_del.setShortcut("del")
button_del.setToolTip("Remove selected files (Del)")

layout.addWidget(listWidget)

layout.addWidget(button_add)
layout.addWidget(button_del)

label_outputsettings = QLabel("Output settings:")
label_outputsettings.setStyleSheet("margin-top:15px")
layout.addWidget(label_outputsettings)
label_outdir=QLineEdit("Same directory as source")
layout.addWidget(label_outdir)

button_outdir = QPushButton('Change &output directory ...')
button_outdir.clicked.connect(change_outdir)
button_outdir.setToolTip("Set output directory (Ctrl+O)")
button_outdir.setShortcut("ctrl+o")
layout.addWidget(button_outdir)

button_settings = QComboBox()
button_settings.addItem("Quicktime 720p")
button_settings.addItem("Quicktime 1080p")
button_settings.addItem("Quicktime 2160p")
button_settings.setStyleSheet("padding:3px")
button_settings.setCurrentIndex(1)
#layout.addWidget(button_settings)

button_submit = QPushButton("Start conversion")
button_submit.clicked.connect(convertall)
button_submit.setToolTip("Starts converting the imported files (Ctrl+Return)")
button_submit.setShortcut("ctrl+return")
button_submit.setStyleSheet("outline:none;padding:15px;text-transform:uppercase;font-size:15px;margin-top:20px")
layout.addWidget(button_submit)

window.setLayout(layout)
window.show()

app.exec_()

