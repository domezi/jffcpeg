import subprocess

def start_detecting():

    old_blk = ""
    print("please plug in device")

    i = 0

    while True:
        
        i+=1
        print("listening "+str(i))

        new_blk=(subprocess.check_output(['ls','-1a','/dev/disk/by-uuid/']))

        #new_blk=(subprocess.check_output(['blkid']))

        if new_blk != old_blk and old_blk != "":
            print("difference detected !!!!")
            new_blk_lines = new_blk.splitlines()
            diff_blk = []

            for line in new_blk_lines:
                print("checking line : "+str(line))
                print("against : "+str(new_blk))
                if line not in old_blk:
                    print("------ This is new !!!")
                    diff_blk.append(line)
                else:
                    print("------ already there :(")

            if len(diff_blk) > 0 :
                print("================")
                print(new_blk)
                print("================")
                show_detected(diff_blk)
                break

        old_blk=new_blk

def show_detected(diff_blks):
    print("*******************")
    print("your devices are")
    print(diff_blks)

    uuid = str(diff_blks[0],"utf-8") #iterate through all blks for multiple partitions

    tmp = None
    while tmp == None or "/" not in tmp :
        tmp=get_mountpoint_by_uuid(uuid)
        
    print("the uuid is: "+uuid)
    print("the mountpoint is: "+tmp)


def get_mountpoint_by_uuid(uuid):
    #out=subprocess.Popen(['lsblk','/dev/disk/by-uuid/'+str(uuid,"UTF-8")])
    out=subprocess.check_output(['lsblk','-o','mountpoint,size','/dev/disk/by-uuid/'+uuid])
    print("lsblk output:")
    print("*******************")
    print("*******************")
    print("*******************")
    print("*******************")
    try:
        out=str(out,"utf-8")
        print(out)
        out=out.splitlines()
        out=out[1].split()
        print("the size is: "+out[1])
        print("the mountpoint should be is: "+out[0])
        return out[0];
    except:
        return None

print("plug out device")
start_detecting()
